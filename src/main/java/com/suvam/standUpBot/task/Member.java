package com.suvam.standUpBot.task;

import net.dv8tion.jda.api.entities.User;

import java.util.ArrayList;
import java.util.List;

public class Member {
    private User user;
    private int questionCount;
    private String name;
    private List<String> questions=new ArrayList<>();

    public Member(User user){
        this.user=user;
        this.questionCount=0;
        this.name=user.getName();
        questions.add("q1");
        questions.add("q2");
        questions.add("q3");
        questions.add("q4");
        this.privateChat();
    }

    public void privateChat(){
        int questionNumber=questionCount++;
     //   System.out.println("questionNumber is "+questionNumber);
        while(questionNumber<4) {
            user.openPrivateChannel().queue(channel -> channel.sendMessage("hello student, " + questions.get(questionNumber)).queue());
            break;
        }
    }

}
