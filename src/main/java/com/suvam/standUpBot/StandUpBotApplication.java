package com.suvam.standUpBot;

import com.suvam.standUpBot.event.UserRepliedEvent;
import com.suvam.standUpBot.task.StandUp;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.ChunkingFilter;
import net.dv8tion.jda.api.utils.MemberCachePolicy;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

@SpringBootApplication
public class StandUpBotApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(StandUpBotApplication.class, args);

		String token="NzkzNDM1NzY2OTY0ODc5Mzgw.X-sOpA.ZalSCiNH_zP84AaChlySmdiaZQg";
		JDA checkJda=JDABuilder.createDefault(token).setChunkingFilter(ChunkingFilter.ALL)
				.setMemberCachePolicy(MemberCachePolicy.ALL).enableIntents(GatewayIntent.GUILD_MEMBERS)
				.setActivity(Activity.watching("morning standup"))
				.addEventListeners(new UserRepliedEvent())
				.build().awaitReady();

		Timer timer=new Timer();
		TimerTask timerTask=new StandUp(checkJda);
		timer.schedule(timerTask,0,9990000);
	}
}
